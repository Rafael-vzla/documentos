puts 'Hello'

=begin
  Problema del tablero de ajedrez:
si en la primera casilla ponemos un grano,
y duplicamos la cantidad de granos en la siguiente,
y as� hasta rellenar el tablero,
�cu�ntos granos tendremos?
=end
 
granos = 1  
3.times do |ciclo|  
puts "En el ciclescaque #{ciclo+1} hay #{granos} granos" 
granos *= 2  
end

#minutos = 60*60*24*365
puts "en un ao hay #{60*60*24*365} minutos"
puts "Hola mundo" 
# Se puede usar " o ' para los strings, pero ' es m�s eficiente. 
puts 'Hola mundo'  
# Juntando cadenas  
puts 'Me gusta' + ' Ruby'  
# Secuencia de escape 
puts 'Ruby\'s party'
# Repetici�n de strings
puts 'Hola' * 3  
# Definiendo una constante  
# M�s adelante se hablar� de constantes  
PI = 3.1416  
puts PI

#puts `dir`

def di_adios(nombre)
  resultado = "Buenas noches, #{nombre} parte uno"
  return resultado
end
 
puts di_adios('Pepe')
 
=begin
  Como los m�todos devuelven el valor
  de la �ltima l�nea, no hace falta 
  el return.
=end
 
def di_adios2(nombre)
  resultado = 'Buenas noches, ' + nombre + ' parte dos'
end
puts di_adios2('Pepe')
 
=begin
  Ahora, en vez de usar ", usamos ',
  utilizando la concatenaci�n de strings
  para obtener el mismo resultado.
=end


string = "Esto es una cadena.."
puts "el largo es #{string.length}" # => 18

# Definici�n de una constante  
PI = 3.1416  
puts PI  
# Definici�n de una variable local  
myString = 'Yo amo mi ciudad, Vigo'  
puts myString  
 
=begin
Conversiones  
to_i - convierte a n�mero entero
to_f - convierte a n�mero decimal
to_s - convierte a string  
=end
 
 
 puts self
 
var1 = 5  
var2 = '2' #fijarse que es un texto 
puts var1 + var2.to_i  
 
=begin
<< marca el comienzo de un string 
    y es seguido de ' o ''. Aqu� a�adimos
    el string junto con el retorno de carro (\n). 
=end
 
a = 'molo '  
a<<'mucho. 
Molo mazo...'  
puts a  
 
=begin
    ' o " son los delimitadores de un string.
    En este caso, podemos sustituirlos por END_STR.
    END_STR es una constante delimitador de strings.
=end
 
# gets y chomp  
#puts "En que ci22udad te gustaria vivir?"  
#STDOUT.flush  
#ciudad = gets.chomp
#puts "La ciudad es " + ciudad


puts "Grados F?"  
STDOUT.flush  
_gf = gets.chomp
puts self
puts _gf + ' grados F son ' + format("%.2f", (_gf.to_f - 32) / 1.8) + ' grados C'

puts "#{_gf} grados F son #{format("%.2f", (_gf.to_f - 32) / 1.8)} grados C"

#puts format("%.2f", x)
puts self

class TS  
  puts 'Comenzo la clase TS'  
  puts self  
  module MTT  
    puts 'Modulo anidado TS::MTT'  
    puts self  
  end  
  puts 'De regreso en el nivel mas superficial de TS'  
  puts self  
end

# Definici�n de un m�todo
def hello  
  puts 'Hola'  
 end  
 #uso del m�todo
 hello  
 
 # M�todo con un argumento
def hello1(nombre)  
  puts 'Hola1 ' + nombre  
  return 'correcto1'  
end  
puts(hello1('Pedro1'))  
 
# M�todo con un argumento (sin par�ntesis, no funciona en versiones nuevas) 
def hello2 nombre2
  puts 'Hola2 ' + nombre2  
  return 'correcto2'  
end  
puts(hello2 'Juan2')


a = "En una lugar de la mancha"
 
#m�todo sin bang: el objeto no se modifica
b = a.upcase
puts b
puts a
 
#m�todo con bang: el objeto se modifica
c = a.upcase!
puts c
puts a


def foo(*mi_string)
  mi_string.each do |palabras|
    puts palabras
  end
end
 
foo('hola', 'mundo', 'muindo')

foo()

=begin
Ejemplo de como los argumentos se
interpretan de izquierda a derecha
=end
 
def mtd(a=99, b=a+1)  
  [a,b]  
end  
puts mtd

nums = -1...9
puts nums.include?(5)   # true
puts nums.min           # -1
puts nums.max           # 8
puts nums.reject {|i| i < 5} # [5, 6, 7, 8]

ciudades = %w{ Pune Mumbai Bangalore }
ciudades.each do |ciudad|
  puts 'Me gusta ' + ciudad + '!'
  puts 'A ti no?'
end
 
#El m�todo {{**delete**}} borra un elemento
ciudades.delete('Mumbai')
ciudades.each do |ciudad|
  puts 'Me gustaba '+ciudad+'!'
  puts 'A ti ya no?'
end

vec = [34, 12, 1, 38]
puts vec.sort
puts vec.length
puts vec.first
puts vec.last








